.PHONY: run
run: target/bfi
	target/bfi src/range_error.b
	echo 127 | target/bfi src/atoi.b
	target/bfi src/div10.b 123
	target/bfi src/hello.b
	target/bfi src/mul10.b 12
	echo 41 | target/bfi src/prime.b
	target/bfi src/bench.b
	target/bfi src/mandel.b

target/bfi: bfi.c | target
	$(CC) -Wall -Wextra -pedantic -Wcast-align -g -o $@ $^

target:
	mkdir -p target

.PHONY: clean
clean:
	-rm -rf target
